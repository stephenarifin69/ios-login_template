//
//  Config.swift
//  ios-login_template
//
//  Created by Stephen Arifin on 4/17/16.
//  Copyright © 2016 Stephen Arifin. All rights reserved.
//

import Foundation

struct Config {
  
  static let DOMAIN_NAME = "http://localhost:3000"
  
  // OPro config values obtained by "localhost:3000/oauth_docs" from rails-login_template_api
  static let CLIENT_ID = "a7d2063082cef07286d91aebaa3858c5"
  static let CLIENT_SECRET = "80ed109bad4934b6f435b67958a0f0bb"
}
