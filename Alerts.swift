//
//  Alerts.swift
//  ios-login_template
//
//  Created by Stephen Arifin on 4/18/16.
//  Copyright © 2016 Stephen Arifin. All rights reserved.
//

import Foundation
import UIKit

class Alerts {
  
  static func show(vc: UIViewController, title: String, message: String) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
    vc.presentViewController(alert, animated: true, completion: nil)
  }
  
}