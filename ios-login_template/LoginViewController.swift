//
//  LoginViewController.swift
//  ios-login_template
//
//  Created by Stephen Arifin on 4/17/16.
//  Copyright © 2016 Stephen Arifin. All rights reserved.
//

import Alamofire
import SwiftyJSON
import UIKit

class LoginViewController: UIViewController {
  
  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  
  private let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
  
  // MARK: View Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
  }
  
  // MARK: User Interaction
  
  @IBAction func login(sender: AnyObject) {
    // Hides keyboard
    UIApplication.sharedApplication().sendAction(#selector(UIResponder.resignFirstResponder), to:nil, from:nil, forEvent:nil)
    activityIndicator.startAnimating()
    
    var parameters = [String: String]()
    parameters["email"] = emailTextField.text
    parameters["password"] = passwordTextField.text
    parameters["client_id"] = Config.CLIENT_ID
    parameters["client_secret"] = Config.CLIENT_SECRET
    
    Alamofire.request(.POST, "\(Config.DOMAIN_NAME)/oauth/token", parameters: parameters).responseJSON {
      response in
      
      print("Success: \(response.result.isSuccess)")
      print("Response String: \(response.result.value)")
      
      if let httpError = response.result.error {
        let statusCode = httpError.code
        print("Status Code: \(statusCode)")
        Alerts.show(self, title: "Login failed", message: "A network error has occured")
      } else { // Success
        let statusCode = (response.response?.statusCode)!
        print("Status code: \(statusCode)")
        
        switch statusCode {
        case 200:
          if let result = response.result.value {
            let json = JSON(result)
            
            // Get current user data and store access token
            let session = Session.getInstance()
            session.setAccessToken(json["access_token"].stringValue)
            session.currentUser = User(json: json["user"])
            
            self.persistSession(json["email"].string, accessToken: json["access_token"].string)
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
              self.activityIndicator.stopAnimating()
              self.leaveLoginScreen()
              return
            })
            
          } else {
            Alerts.show(self, title: "Login failed", message: "An error parsing the request has occured")
          }
        case 401:
          Alerts.show(self, title: "Login failed", message: "Invalid email and password combination")
        default:
          Alerts.show(self, title: "Login failed", message: "\(statusCode) Error")
        }
      }
      self.activityIndicator.stopAnimating()
    }
  }
  
  // MARK: Helper Methods
  
  private func persistSession(email: String?, accessToken: String?) {
    self.defaults.setObject(email, forKey: "email")
    self.defaults.setObject(accessToken, forKey: "accessToken")
    self.defaults.synchronize()
  }

  private func leaveLoginScreen() {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let vc = storyboard.instantiateViewControllerWithIdentifier("mainViewController")
    self.presentViewController(vc, animated: true, completion: nil)
  }
}
