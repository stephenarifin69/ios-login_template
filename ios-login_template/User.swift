//
//  User.swift
//  ios-login_template
//
//  Created by Stephen Arifin on 4/18/16.
//  Copyright © 2016 Stephen Arifin. All rights reserved.
//

import Foundation
import SwiftyJSON

class User {
  
  var id: Int?
  var email: String?
  
  init(json: JSON) {
    id = json["id"].int
    email = json["email"].string
  }
  
}
