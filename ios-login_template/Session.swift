//
//  Session.swift
//  ios-login_template
//
//  Created by Stephen Arifin on 4/18/16.
//  Copyright © 2016 Stephen Arifin. All rights reserved.
//

import Foundation

class Session {
  
  let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
  
  var currentUser: User?
  
  private static var session: Session?
  private var accessToken: String?
  
  static func getInstance() -> Session {
    if let currentSession = self.session {
      return currentSession
    } else {
      session = Session()
      return session!
    }
  }
  
  func setAccessToken(accessToken: String) {
    configuration.HTTPAdditionalHeaders = [
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "token \(accessToken)"
    ]
    self.accessToken = accessToken
  }
  
}
